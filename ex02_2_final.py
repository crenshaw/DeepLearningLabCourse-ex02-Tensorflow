	### ex02 1
# Fabien Jenne
# based on the excellent tutorial provided below

"""A deep MNIST classifier using convolutional layers.
See extensive documentation at
https://www.tensorflow.org/get_started/mnist/pros
"""
# Disable linter warnings
# pylint: disable=invalid-name
# pylint: disable=g-bad-import-order

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import tempfile
import time

import tensorflow as tf
import numpy as np

FLAGS = None


# defs
GPU_ENABLE = 1  # use 'CUDA_VISIBLE_DEVICES="" python3 ex02_2.py' to hide GPU completely
FILTERS_CONV1 = 256
FILTERS_CONV2 = FILTERS_CONV1
KERNELSIZE_CONV1 = 3
KERNELSIZE_CONV2 = KERNELSIZE_CONV1
LEARNINGRATE = 1e-1
FULLYCONN_FEATURES = 128
BATCHES = 10000
BATCHSIZE = 50
OUTPUTSTRIDE = 100
DROPOUT_ENABLE = 0
BATCH_VALIDATION = 1  # [0/1] > set if OOM errors
BATCHES_VALIDATION = 100
BATCH_TESTDATA = 1  # [0/1] > set if OOM errors
BATCHES_TEST = 100  # test data consists of 5000 entries (BATCHES_TEST*BATCHSIZE = 5000)
IMPORT_DATA_MANUALLY = 0  # "= 1" not fully implemented up to now
if ~IMPORT_DATA_MANUALLY:
	from tensorflow.examples.tutorials.mnist import input_data  # imports also numpy



# import the data manually  / not checked up to now
# should not be compatible with the code, since "input_data" provides functions as "next_batch" 
def mnist(datasets_dir='./data'):
    if not os.path.exists(datasets_dir):
        os.mkdir(datasets_dir)
    data_file = os.path.join(datasets_dir, 'mnist.pkl.gz')
    if not os.path.exists(data_file):
        print('... downloading MNIST from the web')
        try:
            import urllib
            urllib.urlretrieve('http://google.com')
        except AttributeError:
            import urllib.request as urllib
        url = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
        urllib.urlretrieve(url, data_file)

    print('... loading data')
    # Load the dataset
    f = gzip.open(data_file, 'rb')
    try:
        train_set, valid_set, test_set = cPickle.load(f, encoding="latin1")
    except TypeError:
        train_set, valid_set, test_set = cPickle.load(f)
    f.close()

    test_x, test_y = test_set
    test_x = test_x.astype('float32')
    #test_x = test_x.astype('float32').reshape(test_x.shape[0], 1, 28, 28)
    test_y = test_y.astype('int32')
    valid_x, valid_y = valid_set
    valid_x = valid_x.astype('float32')
    #valid_x = valid_x.astype('float32').reshape(valid_x.shape[0], 1, 28, 28)
    valid_y = valid_y.astype('int32')
    train_x, train_y = train_set
    #train_x = train_x.astype('float32').reshape(train_x.shape[0], 1, 28, 28)
    train_y = train_y.astype('int32')
    rval = [(train_x, train_y), (valid_x, valid_y), (test_x, test_y)]
    print('... done loading data')
    return rval


# load and partition the data manually
def loadData():
	# load
	Dtrain, Dval, Dtest = mnist()
	X_train, Y_train = Dtrain

	print('... splitting data')
	# Downsample training data to make it a bit faster for testing this code
	n_train_samples = 50000
	train_idxs = np.random.permutation(X_train.shape[0])[:n_train_samples]
	X_train = X_train[train_idxs]
	Y_train = Y_train[train_idxs]

	# create validation data
	X_validation, Y_validation = Dval
	n_val_samples = 10000
	val_idxs = np.random.permutation(X_validation.shape[0])[:n_val_samples]
	X_validation = X_validation[val_idxs]
	Y_validation = Y_validation[val_idxs]

	# create test data
	X_test, Y_test = Dtest
	n_test_samples = 10000
	test_idxs = np.random.permutation(X_test.shape[0])[:n_test_samples]
	X_test = X_test[test_idxs]
	Y_test = Y_test[test_idxs]

	# reshape the data for the calculation of the classification error / obsolete here
	#X_train = X_train.reshape(X_train.shape[0], -1)
	#X_validation = X_validation.reshape(X_validation.shape[0], -1)
	#X_test = X_test.reshape(X_test.shape[0], -1)
	print('... done formatting data')
	return [(X_train, Y_train), (X_validation, Y_validation), (X_test, Y_test)]


# define the NN
def deepnn(x):

	# reshape the X data
	with tf.name_scope('reshape'):
		x_image = tf.reshape(x, [-1, 28, 28, 1])

	# First convolutional layer / ReLu activation
	with tf.name_scope('conv1'):
		W_conv1 = weight_variable([KERNELSIZE_CONV1, KERNELSIZE_CONV1, 1, FILTERS_CONV1])
		b_conv1 = bias_variable([FILTERS_CONV1])
		h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)

	# Pooling layer - downsamples by 2X.
	with tf.name_scope('pool1'):
		h_pool1 = max_pool_2x2(h_conv1)

	# Second convolutional layer / also ReLu activation / weight_variable(kernel_x, kernel_y, no_inputs, no_outputs)
	with tf.name_scope('conv2'):
		W_conv2 = weight_variable([KERNELSIZE_CONV2, KERNELSIZE_CONV2, FILTERS_CONV1, FILTERS_CONV2])
		b_conv2 = bias_variable([FILTERS_CONV2])
		h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)

	# Second pooling layer.
	with tf.name_scope('pool2'):
		h_pool2 = max_pool_2x2(h_conv2)

	# Fully connected layer 1 -- after 2 round of downsampling, our 28x28 image
	# is down to 7x7x16 feature maps -- maps this to 128 features.
	with tf.name_scope('fc1'):
		W_fc1 = weight_variable([7 * 7 * FILTERS_CONV2, FULLYCONN_FEATURES])
		b_fc1 = bias_variable([FULLYCONN_FEATURES])

		h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*FILTERS_CONV2])
		h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

	# Dropout - controls the complexity of the model, prevents co-adaptation of
	# features.
	with tf.name_scope('dropout'):
		keep_prob = tf.placeholder(tf.float32)
		if DROPOUT_ENABLE:
			h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
		else:
			h_fc1_drop = h_fc1

	# classification layer
	# Map the 128 features to 10 classes, one for each digit
	with tf.name_scope('fc2'):
		W_fc2 = weight_variable([FULLYCONN_FEATURES, 10])
		b_fc2 = bias_variable([10])

		y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
	return y_conv, keep_prob


def conv2d(x, W):
	"""conv2d returns a 2d convolution layer with full stride."""
	return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
	"""max_pool_2x2 downsamples a feature map by 2X."""
	return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
							strides=[1, 2, 2, 1], padding='SAME')


def weight_variable(shape):
	"""weight_variable generates a weight variable of a given shape."""
	initial = tf.truncated_normal(shape, stddev=0.1)
	return tf.Variable(initial)


def bias_variable(shape):
	"""bias_variable generates a bias variable of a given shape."""
	initial = tf.constant(0.1, shape=shape)
	return tf.Variable(initial)


def main(_):			
	# Import data
	if IMPORT_DATA_MANUALLY:
		mnist = loadData()
	else:
		mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)

	# Create the model
	x = tf.placeholder(tf.float32, [None, 784])  # 28*28=784

	# Define loss and optimizer
	y_ = tf.placeholder(tf.float32, [None, 10])  # no labels

	# Build the graph for the deep net
	y_conv, keep_prob = deepnn(x)

	# use softmax cross-entropy loss
	with tf.name_scope('loss'):
		cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv)
	cross_entropy = tf.reduce_mean(cross_entropy)

	# train using stochastic gradient descent
	with tf.name_scope('gradient_deescent_optimizer'):
		train_step = tf.train.GradientDescentOptimizer(LEARNINGRATE).minimize(cross_entropy)
	
	# prediction error = accuracy
	with tf.name_scope('accuracy'):
		correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
		correct_prediction = tf.cast(correct_prediction, tf.float32)
	accuracy = tf.reduce_mean(correct_prediction)

	graph_location = tempfile.mkdtemp()
	print('Saving graph to: %s' % graph_location)
	train_writer = tf.summary.FileWriter(graph_location)
	train_writer.add_graph(tf.get_default_graph())
	
	accuracy_training = []
	accuracy_validation = []

	print('\nhyperparams:\nkernel size 1:\t%g\n# filters 1:\t%g\nkernel size 2:\t%g\n# filters 2:\t%g\nlearning rate:\t%g\n# batches:\t%g\nbatch size:\t%g\ndropout:\t%g\nbatch valset:\t%g\nbatch testset:\t%g\nGPU mode:\t%g' % (KERNELSIZE_CONV1, FILTERS_CONV1, KERNELSIZE_CONV2, FILTERS_CONV2, LEARNINGRATE, BATCHES, BATCHSIZE, DROPOUT_ENABLE, BATCH_VALIDATION, BATCH_TESTDATA, GPU_ENABLE))
	print('train ...')
	

	time_start = time.time()
	# use batches > stochastic gradient descent (SGD)
	with tf.Session() as sess:
		sess.run(tf.global_variables_initializer())
		for i in range(BATCHES):	
			batch = mnist.train.next_batch(BATCHSIZE)
			if i % OUTPUTSTRIDE == 0:  # output each 100 training steps
				# calculate training/validation accuracy for each batch
				train_accuracy = accuracy.eval(feed_dict={x: batch[0], y_: batch[1], keep_prob: 1.0})
				# HOTFIX 2: batch the validation set if necessary > prevent OOM errors
				validation_accuracy = []
				if BATCH_VALIDATION:
					for j in range(BATCHES_VALIDATION):
						batch_validation = mnist.validation.next_batch(BATCHSIZE)
						validation_accuracy.append(accuracy.eval(feed_dict={x: batch_validation[0], y_: batch_validation[1], keep_prob: 1.0}))
					validation_accuracy = np.mean(np.asarray(validation_accuracy), axis=0)  # might be more stable than just sum(test_accuracy)/BATCHES_test?
				else:
					validation_accuracy = accuracy.eval(feed_dict={x: mnist.validation.images, y_: mnist.validation.labels, keep_prob: 1.0})
				print(('step {:>' + str(len(str(BATCHES))) + 'd} | \ttraining accuracy {:.6f} \tvalidation accuracy {:.6f}').format(i, train_accuracy, validation_accuracy))
				# append the errors for output
				accuracy_training.append(train_accuracy)
				accuracy_validation.append(validation_accuracy)
		
			# run the train step once for EACH batch
			train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})

		time_end = time.time()
		# calculate test accuracy at the end
		# HOTFIX 1: create testdata batches to prevent OOM error in tensorflow
		if BATCH_TESTDATA:
			print('batching testset ...')
			test_accuracy = []
			for i in range(BATCHES_TEST):
				batch_test = mnist.test.next_batch(BATCHSIZE)
				test_accuracy.append(accuracy.eval(feed_dict={x: batch_test[0], y_: batch_test[1], keep_prob: 1.0}))
			test_accuracy = np.mean(np.asarray(test_accuracy), axis=0)  # might be more stable than just sum(test_accuracy)/BATCHES_test?
		else:
			test_accuracy = accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0})
		
		print('test accuracy {:.6f}'.format(test_accuracy))
		time_elapsed = time_end - time_start
		print('training time {:f} s'.format(time_elapsed))
	
	# write NN stats to file
	with open('accuracy.txt', 'w') as f:
		# prepare the data for export
		#data_out = np.array([np.asarray(accuracy_training), np.asarray(accuracy_validation)])
		print("training accuracy:", file=f)
		for accuracy in accuracy_training:
			print(accuracy, file=f)  # Python 3.x
		print("\nvalidation accuracy:", file=f)
		for accuracy in accuracy_validation:
			print(accuracy, file=f)
		print("\ntest accuracy:", file=f)
		print(test_accuracy, file=f)
		print('\ntraining time {:f} s\n'.format(time_elapsed), file=f)
		print('\nhyperparams:\nkernel size 1:\t%g\n# filters 1:\t%g\nkernel size 2:\t%g\n# filters 2:\t%g\nlearning rate:\t%g\n# batches:\t%g\nbatch size:\t%g\ndropout:\t%g\nbatch valset:\t%g\nbatch testset:\t%g\nGPU mode:\t%g' % (KERNELSIZE_CONV1, FILTERS_CONV1, KERNELSIZE_CONV2, FILTERS_CONV2, LEARNINGRATE, BATCHES, BATCHSIZE, DROPOUT_ENABLE, BATCH_VALIDATION, BATCH_TESTDATA, GPU_ENABLE), file=f)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--data_dir', type=str,
						default='/tmp/tensorflow/mnist/input_data',
						help='Directory for storing input data')
	FLAGS, unparsed = parser.parse_known_args()
	if GPU_ENABLE:
		tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
	else:
		with tf.device("/cpu:0"):  # use to compute the context on CPU only
			tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
